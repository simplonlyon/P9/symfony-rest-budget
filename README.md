# API Budget pour tp
Une simple api de gestion de budget pour exercice front

## How To Use
1. Cloner le projet
2. Créer un .env.local avec les information de connexion bdd dedans
3. Faire la migration : `bin/console doctrine:migrations:migrate`
4. Charger les fixtures : `bin/console doctrine:fixtures:load`
5. Endpoints disponibles : `/api/account` `/api/operation` `/api/category` 